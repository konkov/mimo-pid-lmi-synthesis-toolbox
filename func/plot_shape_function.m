%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fig] = plot_shape_function(shape_functions,opt)
%PLOT_SHAPE_FUNCTION 
arguments (Repeating)
    shape_functions
end
arguments
    opt.data_units {mustBeMember(opt.data_units, {'mag','dB'})} = 'mag'
    opt.freq_units {mustBeMember(opt.freq_units, {'rad/s','Hz'})} = 'Hz'
    opt.XLim = NaN
    opt.YLim = NaN
end

fig = figure;
ax = axes(fig);
ax.XScale = 'log';
ax.Box = 'on';
ax.XGrid ="on";
ax.YGrid ="on";

ax.XLimitMethod = "padded";
ax.YLimitMethod = "padded";


hold(ax,"on")
for i = 1:1:length(shape_functions)
    shape_function = shape_functions{i};
    switch opt.freq_units
        case 'Hz'
            if strcmpi(shape_function.param.freq_units,'rad/s')
                shape_function.freq = shape_function.freq/(2*pi);
            end
            ax.XLabel.String = 'Frequency (Hz)';
        case 'rad/s'
            if strcmpi(shape_function.param.freq_units,'Hz')
                shape_function.freq = shape_function.freq*(2*pi);
            end
            ax.XLabel.String = 'Frequency (rad/s)';
    end

    switch opt.data_units
        case 'mag'
            if strcmpi(shape_function.param.data_units,'dB')
                shape_function.data = db2mag(shape_function.data);
            end
            ax.YLabel.String = 'Singular values (mag)';
            ax.YScale = 'log';
        case 'dB'
            if strcmpi(shape_function.param.data_units,'mag')
                shape_function.data = mag2db(shape_function.data);
            end
            ax.YLabel.String = 'Singular values (dB)';
            ax.YScale = 'linear';
    end


    plot(ax,shape_function.freq,shape_function.data,LineStyle="-.");
end
hold(ax,"off")

cmap = lines(7);
cmap(3,:)= [];
ax.ColorOrder = cmap;

if ~isnan(opt.XLim)
    ax.XLim = opt.XLim;
end
if ~isnan(opt.YLim)
    ax.YLim = opt.YLim;
end

