%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fig] = plot_CCP_results(result,opt)
%PLOT_CCP_RESULTS Results of the Convex-Concave Procedure
%   Detailed explanation goes here
arguments
    result
    opt.MarkerSize = 7;
    opt.mu_scale    {mustBeMember(opt.mu_scale, {'log','linear'})} = 'linear'
    opt.SLFS_scale  {mustBeMember(opt.SLFS_scale, {'log','linear'})} = 'linear'
end

N_models = size(result.P,3);

if N_models > 1
    flag_model_arr = false;
    N_ax = 2;
    n_label = '_n';
else
    flag_model_arr = true;
    N_ax = 3;
    n_label = '';
end



fig = figure;
tLay = tiledlayout(fig,N_ax,1);

ax   = gobjects(N_ax,1);
for i = 1:1:N_ax
    ax(i) = nexttile(tLay,i);
end

hold(ax,"on")
iterations = 1:1:length(result.PID);
plot(    ax(1), iterations, [result.PID.StaticSens_norm]    ,':.', MarkerSize=opt.MarkerSize);
if (result.P.Ts ~= 0)
    ax(1).YLabel.String = {sprintf('$\\|(P%s(1)K_IT_s)^{-1}\\|_2$',n_label),'(mag)'};
else
    ax(1).YLabel.String = {sprintf('$\\|(P%s(0)K_I)^{-1}\\|_2$',n_label),'(mag)'};
end
plot(    ax(2), iterations, ([result.PID.mu])               ,':.', MarkerSize=opt.MarkerSize);
ax(2).YLabel.String = '$\mu_i$ (mag)';

if flag_model_arr
    plot(    ax(3), iterations, cell2mat({result.PID.SetTimes}'),':.', MarkerSize=opt.MarkerSize);
    ax(3).YLabel.String = 'Settling Times (s)';
end

hold(ax,"off")
ax(1).YScale = opt.SLFS_scale;
ax(2).YScale = opt.mu_scale;


for i = 1:1:N_ax
    ax(i).YLabel.Interpreter = 'latex';
    ax(i).XLabel.Interpreter = 'latex';
end

% tLay.TileSpacing = 'none'; 
tLay.TileSpacing = 'compact';
set(ax,'XLim',[0.5 iterations(end)+0.5])
% set(ax,'XLimitMethod','padded')
set(ax,'YLimitMethod','padded')
set(ax,'Box','on')






ax(end).XLabel.String = 'Iterations';




for i = 1:1:N_ax
    if  strcmpi(ax(i).YScale,'linear')
        ax(i).YTick = linspace(ax(i).YTick(1),ax(i).YTick(end),5);
    end
end
set(ax(1:end-1),'XTickLabel',[]);

linkaxes(ax,'x'); %
end

