%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [RGA] = get_RGA(TF,w,opt)
%GET_RGA calculate the Relative Gain Array
%   Detailed explanation goes here
arguments
    TF
    w = 0;
    opt.freq_units {mustBeMember(opt.freq_units, {'rad/s','Hz'})} = 'Hz'
end

G_arr = freqresp(TF,w,opt.freq_units);

RGA = NaN(size(G_arr));


for i = 1:1:size(G_arr,3)
    for j = 1:1:size(G_arr,4)
        G = G_arr(:,:,i,j);
        % RGA(:,:,i) =pinv(G').*G;
        RGA(:,:,i,j) = G.*pinv(G.');
    end
end

% G=pck(A,B,C,D) omega=logspace(-2,2,41) Gw=frsp(G,omega)
% RGAw = veval('.*',Gw,vpinv(vtp(Gw)))
% RGAw = veval('.*',G_arr,vpinv(vtp(G_arr)));
% if size(G,1) == size(G,2)
%
% end

end

