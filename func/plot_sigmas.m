%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fig] = plot_sigmas(result,ind,opt)
%PLOT_SIGMAS Summary of this function goes here
%   Detailed explanation goes here
arguments
    result
    ind = []
    opt.fill_bounds {mustBeNumericOrLogical(opt.fill_bounds)}      = true
    opt.show_Q      {mustBeNumericOrLogical(opt.show_Q)}           = false
    opt.show_W      {mustBeNumericOrLogical(opt.show_W)}           = false
    opt.show_labels {mustBeNumericOrLogical(opt.show_labels)}      = true
    opt.data_units  {mustBeMember(opt.data_units, {'mag','dB'})}   = 'mag'
    opt.freq_units  {mustBeMember(opt.freq_units, {'rad/s','Hz'})} = 'Hz'
    opt.XLim = NaN
    opt.YLim = NaN
end

if isfield(result,'PID') && isempty(fieldnames(result.PID))
    fig = [];
    return
end

if isempty(ind)
    ind = length(result.PID);
end
if length(ind) > 1
    opt.fill_bounds = false;
end

if ~opt.show_Q && result.bound.Q_max.flag
    opt.show_Q = true; % show if Q is bounded
end
if ~opt.show_W && result.bound.W_max.flag
    opt.show_W = true; % show if W is bounded
end


Ts      = result.P.Ts;

if Ts == 0
    flag_discrete = false;
else
    flag_discrete = true;
end

q       = size(result.P,1);
N_model = size(result.P,3);

if N_model == 1
    n_label = '';
else
    n_label = '_n';
end
if q == 1
    q_label = '';
else
    q_label = '_i';
end

label_T      = sprintf('$\\overline{\\sigma}(T%s)$',n_label);
label_inv_S  = sprintf('$1/\\overline{\\sigma}(S%s)$',n_label);
label_inv_F = sprintf('$1/\\sigma%s(F%s)$',q_label,n_label);
% label_inv_F = sprintf('$1/\\overline%s{\\sigma}(F%s)$',n_label);
label_L      = sprintf('$\\sigma%s(L%s)$',q_label,n_label);
label_L_max  = sprintf('$\\overline{\\sigma}(L%s)$',n_label);
label_L_min  = sprintf('$\\underline{\\sigma}(L%s)$',n_label);
label_Q  = sprintf('$\\overline{\\sigma}(Q%s)$',n_label);
% label_Q  = sprintf('$\\sigma%s(Q%s)$',q_label,n_label);
label_W  = sprintf('$\\overline{\\sigma}(W%s)$',n_label);
% label_W  = sprintf('$\\sigma%s(W%s)$',q_label,n_label);
label_T_max  = sprintf('$b_T$');
label_inv_S_max  = sprintf('$1/b_S$');
label_Q_max  = sprintf('$b_Q$');
label_W_max  = sprintf('$b_W$');







fig = figure;
ax = axes(fig);
ax.XScale = 'log';
ax.Box = 'on';
ax.YLabel.String = 'Singular values (dB)';
cmap = lines(7);
% cmap(1,:); % blue
% cmap(2,:); % red
% cmap(3,:); % yellow
% cmap(4,:); % purple
% cmap(5,:); % green
% cmap(6,:); % ligth blue
% cmap(7,:); % dark red

% cmap(5,:); % green
color_T =     cmap(4,:); % purple
color_S =     cmap(2,:); % red
color_L =     cmap(3,:); % yellow
color_L_max = cmap(1,:); % blue
color_L_min = cmap(5,:); % green
color_Q     = cmap(6,:); % ligth blue
color_W     = cmap(7,:); % dark red



% w = logspace(-8,pi,500)/Ts; % Набор круговых частот
w = result.freq_arr;

switch opt.freq_units
    case 'Hz'
        f = w./(2*pi); % переход из рад/с в Гц
        ax.XLabel.String = 'Frequency (Hz)';
        label_nyq_freq   = 'f_{nyq}';
        label_cross_freq = 'f_c';
    case 'rad/s'
        f = w;
        ax.XLabel.String = 'Frequency (rad/s)';
        label_nyq_freq   = '$\\omega_{nyq}$';
        label_cross_freq = '$\\omega_c$';
end

switch opt.data_units
    case 'mag'
        dB_flag = false;
        ax.YScale = 'log';
        ax.YLabel.String = 'Singular values (mag)';
    case 'dB'
        dB_flag = true;
        ax.YScale = 'linear';
        ax.YLabel.String = 'Singular values (dB)';
end

hold(ax,'on')

cross_freq = NaN(N_model,q);

sv_L_min = NaN([size(f),N_model]);
label_all = cell([]);
legend_flag = true;
for i_C = ind
    C   = ss(result.PID(i_C).lti);
    for i = 1:1:N_model
        P = ss(result.P(:,:,i));
        TFs = loopsens(P,C);
        if flag_discrete
            F = (dcgain(P)*result.PID(i_C).Ki*Ts) \ tf([1 -1],1,Ts);
        else
            F = (dcgain(P)*result.PID(i_C).Ki)    \ tf([1  0],1);
        end
        %     val_FR_T = freqresp(TFs{i}.To,f,'Hz'); % Значения частнотной передаточной ф-и T
        %     sv_T   = pagesvd(val_FR_T); % SVD
        [sv_T,~] = sigma(TFs.To,w); % сингулярные числа T(z) на выбранных частотах
        [sv_S,~] = sigma(TFs.So,w); % сингулярные числа S(z) на выбранных частотах
        [sv_F,~] = sigma(F,w); % % сингулярные числа SLFS(z) на выбранных частотах
        [sv_L,~] = sigma(TFs.Lo,w); % сингулярные числа L(z) на выбранных частотах
        if opt.show_Q
            [sv_Q,~] = sigma(TFs.CSo,w); % сингулярные числа Q(z) на выбранных частотах
        end
        if opt.show_W
            [sv_W,~] = sigma(TFs.So*P,w); % сингулярные числа W(z) на выбранных частотах
        end
        sv_T_max = sv_T(1,:); % max sigma;
        sv_S_max = sv_S(1,:); % max sigma;
        sv_L_min(1,:,i) = sv_L(end,:); % max sigma;
        for j = 1:1:q
            cross_freq(i,j) = interp1(sv_L(j,:), w, 1);
        end
        %
        %
        % plot(ax,f,magORdb(sv_L(1,:)./sv_L(end,:),dB_flag), "Color",color_L); % Condition number
        % plot(ax,f,magORdb(sv_T(1,:)./sv_T(end,:),dB_flag), "Color",color_T); % Condition number
        % label_all = make_legend(label_all,'$\kappa(T)$',1,legend_flag);

        plot(ax,f,magORdb(sv_T_max,dB_flag), "Color",color_T);
        label_all = make_legend(label_all,label_T,1,legend_flag);
        %
        plot(ax,f,magORdb(1./sv_S_max,dB_flag), "Color",color_S);
        label_all = make_legend(label_all,label_inv_S,1,legend_flag);
        %
        % plot(ax,f,magORdb(   sv_S_max,dB_flag), "Color",color_S);
        % label_all = make_legend(label_all,label_inv_S,1,legend_flag);
        %
        plot(ax,f,magORdb(1./sv_F, dB_flag), "Color",color_S,"LineStyle",":");
        label_all = make_legend(label_all,label_inv_F,q,legend_flag);
        % plot(ax,f,magORdb(   sv_F, dB_flag), "Color",color_S,"LineStyle",":");
        % label_all = make_legend(label_all,label_inv_F,q,legend_flag);
        switch q
            case 1
                plot(ax,f,magORdb(sv_L(1,:),dB_flag),"Color",color_L); % sigma (L)
                label_all = make_legend(label_all,label_L,1,legend_flag);
            case 2
                plot(ax,f,magORdb(sv_L(1,:),dB_flag),      "Color",color_L_max); % sigma_max (L)
                label_all = make_legend(label_all,label_L_max,1,legend_flag);
                plot(ax,f,magORdb(sv_L(end,:),dB_flag),    "Color",color_L_min); % sigma_min (L)
                label_all = make_legend(label_all,label_L_min,1,legend_flag);
            otherwise
                plot(ax,f,magORdb(sv_L(1,:),dB_flag),      "Color",color_L_max); % sigma_max (L)
                label_all = make_legend(label_all,label_L_max,1,legend_flag);
                plot(ax,f,magORdb(sv_L(end,:),dB_flag),    "Color",color_L_min); % sigma_min (L)
                label_all = make_legend(label_all,label_L_min,1,legend_flag);
                plot(ax,f,magORdb(sv_L(2:end-1,:),dB_flag),"Color",color_L); % sigma (L)
                label_all = make_legend(label_all,label_L,q-2,legend_flag);
        end
        if opt.show_Q
            plot(ax,f,magORdb(sv_Q(1,:),dB_flag),   "Color",color_Q);
            label_all = make_legend(label_all,label_Q,1,legend_flag);
        end
        if opt.show_W
            plot(ax,f,magORdb(sv_W(1,:),dB_flag),   "Color",color_W);
            label_all = make_legend(label_all,label_W,1,legend_flag);
        end
        legend_flag = false;
    end
end


plot(ax,f,magORdb(1./result.bound.S_max.data,dB_flag),"Color",color_S,"LineStyle","-.");
label_all = make_legend(label_all,label_inv_S_max,1,true);

plot(ax,f,magORdb(result.bound.T_max.data,dB_flag),"Color",color_T,"LineStyle","-.");
label_all = make_legend(label_all,label_T_max,1,true);

if opt.show_Q && result.bound.Q_max.flag
    plot(ax,f,magORdb(result.bound.Q_max.data,dB_flag),"Color",color_Q,"LineStyle","-.");
    label_all = make_legend(label_all,label_Q_max,1,true);
end
if opt.show_W && result.bound.W_max.flag
    plot(ax,f,magORdb(result.bound.W_max.data,dB_flag),"Color",color_W,"LineStyle","-.");
    label_all = make_legend(label_all,label_W_max,1,true);
end


if opt.fill_bounds && flag_discrete
    [~,ind] = min(sv_L_min(1,end,:),[],3);
    B_P = sv_L_min(1,:,ind); % performance bound
    % rB = sv_T_max; % robustness  bound
    B_R = result.bound.T_max.data; % robustness  bound
    %
    ind = (B_R < 1);
    f_R = f(ind);
    B_R = B_R(ind);
    if ~isempty(f_R)
        A_R = (f_R(end)-f_R(1)) - trapz(f_R,magORdb(B_R,false));
        fprintf('A_R = %.4f\n', A_R);
        area(ax,f_R,magORdb(B_R,dB_flag),magORdb(1,dB_flag),'FaceAlpha',0.1,'FaceColor',color_T,'LineStyle','none')
        label_all = make_legend(label_all,'Robustness',1,true);
    end
    %
    ind = find(B_P < 1,1);
    % ind = (B_P < 1);
    f_P = f(1:ind);
    B_P = B_P(1:ind);
    if ~isempty(f_P)
        A_P = 2*(trapz(f_P,magORdb(B_P,false)) - (f_P(end)-f_P(1)));
        fprintf('A_P = %.4f\n', A_P);
        area(ax,f_P,magORdb(   B_P,dB_flag),magORdb(1,dB_flag),'FaceAlpha',0.1,'FaceColor',color_S,'LineStyle','none')
        area(ax,f_P,magORdb(1./B_P,dB_flag),magORdb(1,dB_flag),'FaceAlpha',0.1,'FaceColor',color_S,'LineStyle','none')
        label_all = make_legend(label_all,'Performance',2,true);
    end
end


% ind = (sv_SS(1,:) < 1);
% area(ax,f(ind),magORdb(1./sv_SS(1,ind)),'FaceAlpha',0.1,'FaceColor',color_S,'LineStyle','none')

if strcmp(opt.freq_units,'Hz')
    cross_freq = cross_freq/(2*pi); % переход из рад/с в Гц
end


[min_cross_freq,max_cross_freq] = bounds(cross_freq,"all");

xline(ax,min_cross_freq,'k--', sprintf('$%s = %.2g $%s', label_cross_freq, min_cross_freq,opt.freq_units)); % частота среза
if (max_cross_freq - min_cross_freq) > 1e-3
    xline(ax,max_cross_freq,'k--', sprintf('$%s = %.2g $%s', label_cross_freq, max_cross_freq,opt.freq_units)); % частота среза
end
if flag_discrete
    xline(ax,f(end),'k-',sprintf('$%s = %.f $%s',label_nyq_freq, f(end),opt.freq_units)); % частота Найквиста
end
constLines = findobj(ax.Children, 'Type','ConstantLine');

set(constLines, ...
    'LabelHorizontalAlignment','center', ...
    'LabelVerticalAlignment','bottom', ...
    'LabelOrientation','aligned', ...
    'Interpreter', 'latex' ...
    );
if ~opt.show_labels
    set(constLines, 'Label', '');
end


legend(ax,label_all,'Interpreter','latex');

hold(ax,'off');

ax.Box = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';


if ~isnan(opt.XLim)
    ax.XLim = opt.XLim;
else
    if flag_discrete
        ax.XLim = [f(1) f(end)*2];
    else
        ax.XLim = [f(1) f(end)];
    end
end
if ~isnan(opt.YLim)
    ax.YLim = opt.YLim;
end

end

function out = magORdb(in,flag)
if flag
    out = mag2db(in);
else
    out = in;
end
end

function out = make_legend(in,label,N,flag)
if flag
    out = horzcat(in,{label},repmat({''},1,N-1));
else
    out = horzcat(in,repmat({''},1,N));
end
end