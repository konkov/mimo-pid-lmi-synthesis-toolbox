%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Kp,Ki,Kd] = init_MIMO_PID(P,init_param,pid_param,show_init)
%INIT_MIMO_PID Synthesis of initial MIMO PID controller of a given type
% alg_param.method - метод синтеза
% pid_param.Ts   - sample time, если 0, то непрерывное время
arguments
    P
    init_param
    pid_param
    show_init = false;
end
% freq_unit = 'rad/s';
% freq_unit = 'cycles/TimeUnit';
if nargout == 0
    show_init = true;
end


switch init_param.init_method
    case "pinv"
        P0_pinv = pinv(dcgain(P));
        Kp = zeros(size(P))';
        % Kp = alg_param.init_epsilon * 3 * P0_pinv;
        % Kp = 1 * pinv(freqresp(P,getGainCrossover(P,1)));
        Ki = init_param.init_epsilon * P0_pinv;
        Kd = zeros(size(P))';
    case "manual"
        Kp = init_param.init_Kp;
        Ki = init_param.init_Ki;
        Kd = init_param.init_Kd;
    otherwise
        error('Ничего не выбрано');
end

loops = loopsens(P,build_MIMO_PID(Kp,Ki,Kd,pid_param));

if isfinite(loops.Stable) && ~loops.Stable
    unstable_flag = true;
else
    unstable_flag = false;
end

if show_init || unstable_flag

    fig = findobj(groot,'Tag','st_resp');
    if isempty(fig)
        fig = figure('Name','Step Response','Tag','st_resp');
        ax = axes(fig);
    else
      ax = findobj(fig(1).Children,'Type','axes');
    end
    h = stepplot(ax(1),loops.To);
    T = getoptions(h,'Title');
    T.String = sprintf('Initial Controller. epsilon = %.e',init_param.init_epsilon);
    setoptions(h,'Title',T);
    figure(fig(1));
    drawnow;
end

if unstable_flag
    error("Система с инициализирующим регулятором неустойчива");
end
end

