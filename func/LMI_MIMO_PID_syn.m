%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [out] = LMI_MIMO_PID_syn(P, CS_param, init_param, PID_param,opt)
%LMI_MIMO_PID_SYN main script for controller synthesis
arguments
    P % Plant model / Array of Plant models
    %
    CS_param.freq_N = 200
    CS_param.freq_int = 'auto'
    CS_param.CCP_max_iter = 10
    CS_param.S_max = 1.3
    CS_param.T_max = 1.3
    CS_param.Q_max = inf
    CS_param.W_max = inf
    CS_param.tol = 1
    CS_param.precision {mustBeMember(CS_param.precision, {'default','low','high','best'})} = 'low'
    CS_param.packet    {mustBeMember(CS_param.packet, {'cvx','yalmip'})}  = 'yalmip'; % cvx yalmip
    CS_param.solver    {mustBeMember(CS_param.solver, {'mosek','sdpt3'})} = 'sdpt3'; %
    %
    init_param.init_method  {mustBeMember(init_param.init_method, {'pinv', 'manual'})} = 'pinv';
    init_param.init_epsilon {mustBeNumeric} = 1e-2;
    init_param.init_Kp      {mustBeNumeric}
    init_param.init_Ki      {mustBeNumeric}
    init_param.init_Kd      {mustBeNumeric}
    %
    PID_param.type     char {mustBeMember(PID_param.type, {'PID','P','I','D','PI','PD','ID'})}                  = 'PID';
    PID_param.filter   char {mustBeMember(PID_param.filter, {'default','hi_freq'})}                             = 'default';
    PID_param.IFormula char {mustBeMember(PID_param.IFormula, {'ForwardEuler', 'BackwardEuler','Trapezoidal'})} = 'BackwardEuler';
    PID_param.DFormula char {mustBeMember(PID_param.DFormula, {'ForwardEuler', 'BackwardEuler','Trapezoidal'})} = 'BackwardEuler';
    PID_param.tau = 0;
    %
    opt.detailed {mustBeNumericOrLogical(opt.detailed)} = true  % turn on if you want to output H-inf norms at each step.
    opt.verbose  {mustBeNumericOrLogical(opt.verbose)}  = false % turn on if you need to see the LMI solution process
    opt.keep_fig {mustBeNumericOrLogical(opt.keep_fig)} = false
    opt.nowarning   {mustBeNumericOrLogical(opt.nowarning)}   = false
    opt.lang     {mustBeMember(opt.lang, {'ru','en'})}  = 'ru'
end
% ncol = matlab.desktop.commandwindow.size; ncol = ncol(1); ncol(2) = round(ncol(1)*2/3); ncol(3) = round(ncol(1)/2);
ncol = [100 75 50];
whole_timer = tic;

switch opt.lang
    case 'ru'
        flag_lang = true;
    case 'en'
        flag_lang = false;
    otherwise
        flag_lang = false;
end

switch CS_param.packet
    case 'cvx'
        flag_cvx_ = true;
        if ~(exist('cvx','file') && exist('cvx_version','file'))
            if flag_lang
                error('Framework CVX не найден. Установите его согласно инструкции http://web.cvxr.com/cvx/doc/install.html и убедитесь что вывод комманды cvx_where находится в MATLAB Path.')
            else
                error('Framework CVX not found. Install it according to the instructions at http://web.cvxr.com/cvx/doc/install.html and make sure that the output of the cvx_where command is in MATLAB Path.')
            end
        end
    case 'yalmip'
        flag_cvx_ = false;
        if ~(exist('yalmip','file') && exist('yalmiptest','file'))
            if flag_lang
                error('Framework YALMIP не найден. Установите его согласно инструкции https://yalmip.github.io/tutorial/installation/.')
            else
                error('Framework YALMIP not found. Install it according to the instructions at https://yalmip.github.io/tutorial/installation/.')
            end
        end
end



if flag_lang
    fprintf('%s\n Настройка MIMO PID-регулятора\n%s\n',repmat('=',1,ncol(1)),repmat('=',1,ncol(1)));
else
    fprintf('%s\n Tuning the MIMO PID controller\n%s\n',repmat('=',1,ncol(1)),repmat('=',1,ncol(1)));
end

% Валидация параметров
Ts = P(:,:,1).Ts;
PID_param.Ts = Ts;
CS_param.Ts  = Ts;

if strcmpi(CS_param.freq_int,'auto')
    [~,~,CS_param.freq_int] = bode(P(:,:,1)); % rad/TimeUnit
    CS_param.freq_int = [CS_param.freq_int(1) CS_param.freq_int(end)];
    flag_auto_freq_int = true;
else
    flag_auto_freq_int = false;
end

if (Ts == 0)
    flag_discrete = false;
    PID_param = rmfield(PID_param,'IFormula');
    PID_param = rmfield(PID_param,'DFormula');
    if PID_param.tau == 0 && contains(PID_param.type,'D','IgnoreCase',true)
        if flag_lang
            error('В непрерывном времени tau должна быть задана.')
        else
            error('In continuous time, tau must be specified.')
        end
    end
else
    flag_discrete = true;
    CS_param.freq_int(2) = (pi/Ts); % для дискретных систем w_max = 2*pi*f_nyquist
end

out = struct;
out.param.CS     = CS_param;
out.param.PID    = PID_param;
out.param.init   = init_param;
out.P = P;
out.PID = struct;

[q, m , N_models] = size(P);


P = ss(P);

if ~opt.nowarning
    flag_bad_model = false;
    for i = 1:N_models
        if size(P(:,:,i).A,1) ~= rank(ctrb(P(:,:,i)))
            if flag_lang
                warning('Объект #%i не управляемый',i)
            else
                warning('The plant #%i is not controllable.',i)
            end
            flag_bad_model = true;
        end
        if size(P(:,:,i).A,1) ~= rank(obsv(P(:,:,i)))
            if flag_lang
                warning('Объект #%i не наблюдаемый',i)
            else
                warning('The plant #%i is not observable.',i)
            end
            flag_bad_model = true;
        end
    end
    if flag_bad_model
        txt = input("Продолжить?/Continue? [y/n]","s");
        if ~strcmpi(txt,'y')
            return;
        end
    end
end
% if isempty(nonzeros(P.IODelay))
%     flag_delayed_sys = false;
% else
%     flag_delayed_sys = true;
% end
flag_delayed_sys = hasdelay(P);


% if flag_discrete && flag_delayed_sys
%     P = absorbDelay(P);
%     P = balreal(P);
% end

if  ~flag_delayed_sys
    for i = 1:N_models
        P(:,:,i) = balreal(P(:,:,i));
    end
end

P_nom = P(:,:,1);


% CS_param.freq_int = deg2rad(CS_param.freq_int);
% freq_unit = 'Hz';
% freq_unit = 'rad/s';

% flagFreqRespMethod
if flag_lang
    disp('Параметры алгоритма')
else
    disp('Algorithm parameters')
end
disp(CS_param);
disp(repmat('+',1,ncol(3)));
if flag_lang
    disp('Параметры регулятора')
else
    disp('Controller parameters')
end
disp(PID_param);
disp(repmat('+',1,ncol(3)));
if flag_lang
    disp('Параметры инициализации')
else
    disp('Initialization parameters')
end
disp(init_param);
disp(repmat('+',1,ncol(2)));


if q>m
    if flag_lang
        warning('Число входов должно быть больше или равно числа выходов')
    else
        warning('The number of inputs must be greater than or equal to the number of outputs')
    end
    txt = input("Продолжить?/Continue? [y/n]","s");
    if ~strcmpi(txt,'y')
        return;
    end
end

if N_models > 1
    if flag_lang
        fprintf('Регулятор будет синтезирован на массиве моделей объекта N = %i\n',N_models);
    else
        fprintf('The controller will be synthesized on the array of plant models N = %i\n',N_models);
    end
end

if flag_lang
    fprintf('Модель объекта имеет m = %i входов и q = %i выходов\n',m,q);
else
    fprintf('The plant model has m = %i inputs and q = %i outputs\n',m,q);
end

if flag_discrete
    if flag_lang
        fprintf('Синтез в дискретном времени. Ts = %.e сек\n',Ts);
    else
        fprintf('Synthesis in discrete time. Ts = %.e sec\n',Ts);
    end
else
    if flag_lang
        fprintf('Синтез в непрерывном времени.\n');
    else
        fprintf('Synthesis in continuous time.\n');
    end
end

if flag_auto_freq_int
    if flag_lang
        fprintf('Автоматическое задание частотного интервала:\n');
    else
        fprintf('Auto setting of frequency interval:\n');
    end
else
    if flag_lang
        fprintf('Частотный интервал задан вручную:\n');
    else
        fprintf('Frequency interval set manually:\n');
    end
end

freq_arr = logspace(log10(CS_param.freq_int(1)),log10(CS_param.freq_int(2)),CS_param.freq_N);
freq_arr_Lim = [freq_arr(1) freq_arr(end)];
% freq_arr = logspace(freq_arr(1),pi,N);
if flag_discrete
    complex_freq_arr = exp(1j*freq_arr*Ts);
else
    complex_freq_arr = 1j*freq_arr;
end
out.freq_arr = freq_arr;

if flag_discrete
    if flag_lang
        fprintf(' от %.3e rad/sec (%.3e Гц) до f_nyquist = pi*%.3e = %.3e рад/сек (%.3e Гц) | %i частот.\n',freq_arr(1),freq_arr(1)/(2*pi),freq_arr(end)/pi,freq_arr(end),freq_arr(end)/(2*pi),CS_param.freq_N);
    else
        fprintf(' from %.3e rad/sec (%.3e Hz) to f_nyquist = pi*%.3e = %.3e rad/sec (%.3e Hz) | %i frequencies.\n',freq_arr(1),freq_arr(1)/(2*pi),freq_arr(end)/pi,freq_arr(end),freq_arr(end)/(2*pi),CS_param.freq_N);
    end
else
    if flag_lang
        fprintf(' от %.3e рад/сек (%.3e Гц) до %.3e рад/сек (%.3e Гц) | %i частот.\n',freq_arr(1),freq_arr(1)/(2*pi),freq_arr(end),freq_arr(end)/(2*pi),CS_param.freq_N);
    else
        fprintf(' from %.3e rad/sec (%.3e Hz) to %.3e rad/sec (%.3e Hz) | %i frequencies.\n',freq_arr(1),freq_arr(1)/(2*pi),freq_arr(end),freq_arr(end)/(2*pi),CS_param.freq_N);
    end
    % fprintf(' от 10^%.3g рад/сек (%.2g Гц) до 10^%.3g рад/сек (%.2g Гц) | %i частот.\n',log10(freq_arr(1)),freq_arr(1)/(2*pi),log10(freq_arr(end)),freq_arr(end)/(2*pi),CS_param.freq_N);
end

if flag_lang
    fprintf('%s\n Максимум %i итераций\n%s\n',repmat('-',1,ncol(2)),CS_param.CCP_max_iter,repmat('-',1,ncol(2)));
else
    fprintf('%s\n Maximum %i iterations\n%s\n',repmat('-',1,ncol(2)),CS_param.CCP_max_iter,repmat('-',1,ncol(2)));
end


out.bound = struct;
for bound_name = ["S_max", "T_max", "Q_max", "W_max"]
    if isstruct(CS_param.(bound_name))
        bounds_param = namedargs2cell(CS_param.(bound_name).param);
        out.bound.(bound_name)  = build_shape_function(freq_arr,bounds_param{:});
        out.bound.(bound_name).flag = true;
    else
        if isfinite(CS_param.(bound_name))
            out.bound.(bound_name) = build_shape_function(freq_arr,type='Constant',H_inf_norm=CS_param.(bound_name));
            out.bound.(bound_name).flag = true;
        else
            out.bound.(bound_name).flag = false;
        end
    end
end
clear bound_name bounds_param



S_max_flag = out.bound.S_max.flag; % для скорости в цикле
T_max_flag = out.bound.T_max.flag;
Q_max_flag = out.bound.Q_max.flag;
W_max_flag = out.bound.W_max.flag;
if  S_max_flag
    S_max_data = out.bound.S_max.data;
end
if  T_max_flag
    T_max_data = out.bound.T_max.data;
end
if  Q_max_flag
    Q_max_data = out.bound.Q_max.data;
end
if  W_max_flag
    W_max_data = out.bound.W_max.data;
end



if opt.detailed
    figStepResp = figure('Name','Step Response','Tag','st_resp');
    axStepResp = axes(figStepResp);
    figStepResp.Name = sprintf('Step Response');
end

mu_arr               = NaN(CS_param.CCP_max_iter,1);
SettlingTime         = NaN(q,N_models);
StaticSens_norm      = NaN(N_models,1);
StaticSens_norm_prev = NaN(N_models,1);
%% Инициализация регулятора

[Kp,Ki,Kd] = init_MIMO_PID(P_nom,init_param,PID_param,opt.detailed);
% PID = PID_built(Kp, Ki, Kd, PID_param);

if opt.detailed && isgraphics(figStepResp)
    figure(figStepResp)
end
P_dcgain = dcgain(P);

for i = 1:1:N_models
    if flag_discrete
        StaticSens_norm_prev(i) = norm(inv(P_dcgain(:,:,i)*Ki*Ts));  % норма статической чувствительности начальная
    else
        StaticSens_norm_prev(i) = norm(inv(P_dcgain(:,:,i)*Ki));     % норма статической чувствительности начальная
    end
end




%% Convex Concave Procedure
for iteration = 1:1:CS_param.CCP_max_iter
    if flag_lang
        fprintf('Итерация №  %i\n',iteration);
        fprintf('Подготовка ...'); prepare_timer = tic;
    else
        fprintf('Iteration # %i\n',iteration);
        fprintf('Preparing ...'); prepare_timer = tic;
    end

    if flag_cvx_ % cvx
        cvx_begin sdp
        cvx_quiet(~opt.verbose)
        cvx_solver(CS_param.solver)
        cvx_precision(CS_param.precision)
        % rmdepconstr
        % настройки CVX
        % options = sdpsettings('solver','sdpa','verbose',2,'showprogress',1,'sdpa.maxIteration',100);
        % options = sdpsettings('solver','sdpt3','verbose',1,'showprogress',1);
        % options.allownonconvex = 0;
        % options.convertconvexquad = 0;
        % % options.savesolveroutput = 1;
        %
        % options.sdpt3.predcorr = 0;
        % options.sdpt3.maxit = 100;
        %     cvx_solver_settings('maxit',500)
        %     cvx_solver_settings('rmdepconstr',1)
        %     OPTIONS.rmdepconstr = 1;
    else % yalmip
        yalmip('clear');
        yalmip_opt = sdpsettings;
        yalmip_opt.removeequalities = true;
        yalmip_opt.solver = CS_param.solver;
        yalmip_opt.usex0 = true;
        yalmip_opt.verbose = opt.verbose;
        % yalmip_opt.showprogress = true;
        yalmip_opt.sdpt3.maxit = 100;
        %
        prefvals = { ...
            [ eps^0.5,   eps^0.5,   eps^0.25  ], ...
            [ eps^0.75,  eps^0.75,  eps^0.375 ], ...
            [ eps^0.5,   eps^0.375, eps^0.25  ], ...
            [ eps^0.375, eps^0.25,  eps^0.25  ], ...
            [ 0,         eps^0.5,   eps^0.25  ] };
        prefnames = { 'default', 'high', 'medium', 'low', 'best' };
        yalmip_opt.sdpt3.steptol = prefvals{strcmpi(CS_param.precision,prefnames)}(2);

        % yalmip_opt.mosek

        % yalmip_opt.solver = 'sdpa';
        % yalmip_opt.solver = 'scs';
        % yalmip_opt.scs.normalize = 0;
        % yalmip_opt.scs.eliminateequalities = 0;
        % yalmip_opt.scs.adaptive_scale = 0;
        %
        % yalmip_opt.solver = 'sedumi';
        % yalmip_opt.sedumi.eps = 1e-3;
        % yalmip_opt.sedumi.cg.qprec = 1;
        % yalmip_opt.sedumi.cg.maxiter =  49;
        % yalmip_opt.sedumi.stepdif = 2;
        %
        % vertcat(prefnames, prefvals)'
        % yalmip_opt.sdpt3.steptol = 1e-4;
        %
        % yalmip_opt.sdpt3.scale_data = true;
        yalmip_opt.sdpt3.rmdepconstr = true;
    end

    if contains(PID_param.type,'P','IgnoreCase',true)
        if flag_cvx_ % cvx
            variable Kp_LMI_var(m,q)
        else % yalmip
            Kp_LMI_var = sdpvar(m,q,'full');
            assign(Kp_LMI_var,Kp);
        end
    else
        Kp_LMI_var = zeros(m,q);
    end
    if contains(PID_param.type,'I','IgnoreCase',true)
        if flag_cvx_ % cvx
            variable Ki_LMI_var(m,q)
        else % yalmip
            Ki_LMI_var = sdpvar(m,q,'full');
            assign(Ki_LMI_var,Ki);
        end
    else
        if flag_lang
            error('Интегратор нужен обязально');
        else
            error('Integrator is mandatory');
        end
        % Ki_LMI_var = zeros(m,q);
    end
    if contains(PID_param.type,'D','IgnoreCase',true)
        if flag_cvx_ % cvx
            variable Kd_LMI_var(m,q)
        else % yalmip
            Kd_LMI_var = sdpvar(m,q,'full');
            assign(Kd_LMI_var,Kd);
        end
    else
        Kd_LMI_var = zeros(m,q);
    end
    %
    if flag_cvx_ % cvx
        variable mu_var
        maximize mu_var;
    else % yalmip
        mu_var = sdpvar(1,1);
        if iteration > 1
            assign(mu_var,mu_arr(iteration-1));
            % else
            %     assign(mu_var,StaticSens_norm_prev(1)^-2);
        end
    end

    I_m  = eye(m);
    I_q  = eye(q);
    for n = 1:1:N_models
        %% min ||(P(0)Ki)^-1||_2
        X_n = P_dcgain(:,:,n)*Ki_LMI_var;
        if flag_discrete
            Y_1 = mu_var * Ts^-2 * I_q;
        else
            Y_1 = mu_var * I_q;
        end
        X_n_cur = P_dcgain(:,:,n)*Ki;
        %% var 1 - как остальные
        % LMI_1 = [Z_1'*Z_cur_1+Z_cur_1'*Z_1-Z_cur_1'*Z_cur_1 Y_1'; Y_1 I_q];
        % LMI_1 >= 0;
        % %% var 2 -  эквивалентная замена
        if flag_cvx_ % cvx
            X_n'*X_n_cur + X_n_cur'*X_n - X_n_cur'*X_n_cur - Y_1 >= 0;
        else % yalmip
            LMI = X_n'*X_n_cur + X_n_cur'*X_n - X_n_cur'*X_n_cur - Y_1 >= 0;
        end

        %% S(s), T(s), Q(s), W(s) для каждой частоты
        for f_ind = 1:1:CS_param.freq_N
            f_k = complex_freq_arr(f_ind);
            %% Подготовка
            % P_k      = freqresp(P(:,:,n),freq_arr(f_ind));
            P_nk     = evalfr(P(:,:,n),f_k);
            %
            C_k      = build_MIMO_PID(Kp_LMI_var,Ki_LMI_var,Kd_LMI_var,PID_param,f_k);
            % C_k_cur = freqresp(PID,freq_arr(f_ind));
            % C_k_cur = evalfr(PID,f_k); %!!!!
            C_k_cur  = build_MIMO_PID(Kp,Ki,Kd,PID_param,f_k);
            %
            Z_nk     = I_q + (P_nk * C_k    );
            Z_nk_cur = I_q + (P_nk * C_k_cur);
            Z_nk_LMI = (Z_nk'*Z_nk_cur) + (Z_nk_cur'*Z_nk) - (Z_nk_cur'*Z_nk_cur);
            %% Функция Чувствительности
            if S_max_flag && ~isnan(S_max_data(f_ind))
                % Y_k_2 = (1/S_max_data(f))*I_q; % [Z_exp Y_k_2'; Y_k_2 I_q] >= 0;
                if flag_cvx_ % cvx
                    Z_nk_LMI - S_max_data(f_ind)^(-2) * I_q >= 0;
                else % yalmip
                    LMI = [LMI, Z_nk_LMI - S_max_data(f_ind)^(-2) * I_q >= 0];
                end
            end
            %% Дополнительная Функция Чувствительности
            if T_max_flag && ~isnan(T_max_data(f_ind))
                Y_nk_3 = (1/T_max_data(f_ind))*P_nk*C_k;
                if flag_cvx_ % cvx
                    [Z_nk_LMI Y_nk_3'; Y_nk_3 I_q] >= 0;
                else % yalmip
                    LMI = [LMI, [Z_nk_LMI Y_nk_3'; Y_nk_3 I_q] >= 0];
                end
            end
            %% Q-параметр
            if Q_max_flag && ~isnan(Q_max_data(f_ind))
                Y_k_4 = (1/Q_max_data(f_ind))*C_k;
                if flag_cvx_ % cvx
                    [Z_nk_LMI Y_k_4'; Y_k_4 I_m] >= 0;
                else % yalmip
                    LMI = [LMI, [Z_nk_LMI Y_k_4'; Y_k_4 I_m] >= 0];
                end
            end
            %% W
            if W_max_flag && ~isnan(W_max_data(f_ind))
                Y_nk_5 = (1/W_max_data(f_ind))*P_nk;
                if flag_cvx_ % cvx
                    [Z_nk_LMI' Y_nk_5; Y_nk_5' I_m]  >= 0;
                else % yalmip
                    LMI = [LMI, [Z_nk_LMI' Y_nk_5; Y_nk_5' I_m]  >= 0];
                end
            end
        end
    end
    if flag_lang
        fprintf('\b\b\bзавершена за %.1f сек.\n',toc(prepare_timer));
    else
        fprintf('\b\b\bcompleted in %.1f sec.\n',toc(prepare_timer));
    end

    if ~opt.verbose
        if flag_lang
            fprintf('Решение LMI ...');
        else
            fprintf('LMI solution ...');
        end
        solver_timer = tic;
    end

    if flag_cvx_ % cvx
        cvx_end
        Kp = Kp_LMI_var;
        Ki = Ki_LMI_var;
        Kd = Kd_LMI_var;
        mu = mu_var;
    else % yalmip
        diagnostics = optimize(LMI,-mu_var,yalmip_opt);
        Kp = value(Kp_LMI_var);
        Ki = value(Ki_LMI_var);
        Kd = value(Kd_LMI_var);
        mu = value(mu_var);
    end

    if ~opt.verbose
        if flag_lang
            fprintf('\b\b\bзавершено за %.1f сек.\n',toc(solver_timer));
        else
            fprintf('\b\b\bcompleted in %.1f sec.\n',toc(solver_timer));
        end
    end

    flag_LMI_has_solution = true;

    if flag_cvx_ % cvx
        disp(cvx_status)
        if strcmpi(cvx_status,'Infeasible')
            flag_LMI_has_solution = false;
        end
    else % yalmip
        disp(diagnostics.info)
        if diagnostics.problem == 1
            flag_LMI_has_solution = false;
        end
    end

    if ~flag_LMI_has_solution
        if flag_lang
            disp('Остановка! Система LMI не имеет решения');
        else
            disp('Stop! The LMI system has no numerical solution');
        end
        break;
    end

    if iteration == 1
        fprintf('\n    mu =  %.5g\n', mu     );
        fprintf(' gamma =  %.5g\n\n', mu^-0.5);
        mu_diff = NaN;
    else
        mu_diff    = round(mu/mu_arr(iteration-1) - 1          , 4)*1e2;
        gamma_diff = round(mu_arr(iteration-1)^-0.5/mu^-0.5 - 1, 4)*1e2;
        fprintf('\n    mu: %.5g --> %.5g, %3.2f %% \n', mu_arr(iteration-1)     , mu     , mu_diff   );
        fprintf(' gamma: %.5g --> %.5g, %3.2f %% \n\n', mu_arr(iteration-1)^-0.5, mu^-0.5, gamma_diff);
    end
    mu_arr(iteration) =  mu;

    for i = 1:1:N_models
        if flag_discrete
            StaticSens_norm(i) = norm(inv(P_dcgain(:,:,i)*Ki*Ts));
        else
            StaticSens_norm(i) = norm(inv(P_dcgain(:,:,i)*Ki));
        end
        fprintf('||(P(0)Ki)^-1||_2: %.5g --> %.5g, %3.2f %% \n\n', StaticSens_norm_prev(i), StaticSens_norm(i), round((StaticSens_norm_prev(i)/StaticSens_norm(i)) - 1, 4) * 1e2);
    end

    % PID_param.tau = 0.5*abs(Kd./Kp);
    % PID_param.tau = norm(Kd)/(2*norm(Kp));

    PID = build_MIMO_PID(Kp, Ki, Kd, PID_param);
    %% Проверка на каждом шаге
    out.PID(iteration).Kp  = Kp;
    out.PID(iteration).Ki  = Ki;
    out.PID(iteration).Kd  = Kd;
    out.PID(iteration).tau = PID_param.tau;
    out.PID(iteration).lti = PID;
    out.PID(iteration).mu  = mu;
    out.PID(iteration).StaticSens_norm = StaticSens_norm;


    if opt.detailed

        % if flag_delayed_sys
        %     if flag_discrete
        %         P_no_del = absorbDelay(P);
        %     else
        %         P_no_del = pade(P);
        %     end
        %     loops = loopsens(P_no_del,PID);
        % else
        %     P_no_del = P;
        %     loops = loopsens(P,PID);
        % end
        loops = loopsens(P,PID);
        if ~flag_delayed_sys
            disp('H∞ - norms:');
            fprintf('||S||∞ = %5.4f\n', getPeakGain(loops.So      ,1e-3,freq_arr_Lim));
            fprintf('||T||∞ = %5.4f\n', getPeakGain(loops.To      ,1e-3,freq_arr_Lim));
            fprintf('||Q||∞ = %5.4e\n', getPeakGain(PID*loops.So  ,1e-3,freq_arr_Lim));
            fprintf('||W||∞ = %5.4e\n', getPeakGain(-1*loops.So*P ,1e-3,freq_arr_Lim));

            fprintf('∑[S_0] = %4.2f\n', abs(sum(dcgain(loops.So)/N_models,'all')));
            fprintf('∑[T_0] = %4.2f\n', abs(sum(dcgain(loops.To)/N_models,'all')));
        end
        % G = feedback(P*PID,eye(q));
        G = loops.To;
        % [y,tOut] = step(G_cl,1e-3)
        clear GstepInfo
        for i = 1:1:N_models
            GstepInfo(:,:,i) = stepinfo(G(:,:,i),'SettlingTimeThreshold',5e-2);
        end
        out.PID(iteration).stepInfo = GstepInfo;
        if flag_lang
            fprintf('Времена переходного процесса по каналам P_1:\n');
        else
            fprintf('Transient times on loops of P_1:\n');
        end
        for i = 1:q
            SettlingTime(iteration,i) = GstepInfo(i,i,1).SettlingTime;
            % SettlingTime(iteration,iii) = stepinfo(G(iii,iii),'SettlingTimeThreshold',5e-2).SettlingTime;
            fprintf(' %i : %g seconds\n', i, SettlingTime(iteration,i));
        end
        out.PID(iteration).SetTimes = SettlingTime(iteration,:);

        if flag_lang
            fprintf('Макс/Мин время переходного процесса: %.6g / %.6g секунд\n',max(SettlingTime(iteration,:)),min(SettlingTime(iteration,:)));
        else
            fprintf('Max/Min transient time: %.6g / %.6g seconds\n',max(SettlingTime(iteration,:)),min(SettlingTime(iteration,:)));
        end
        % figStepResp.Name = sprintf('Step Response interation # %i',iteration);
        % plotoptions = timeoptions;

        % plotoptions.Title.String = sprintf('Step Response interation # %i',iteration);
        % stepplot(axStepResp,G,max(SettlingTime(iteration,:)),plotoptions);

        if isgraphics(figStepResp)
            h = stepplot(axStepResp,G,max(SettlingTime(iteration,:)));
            T = getoptions(h,'Title');
            T.String = sprintf('Step Response interation # %i',iteration);
            setoptions(h,'Title',T);
            figure(figStepResp)
            drawnow;
        end

        if isfinite(loops.Stable) && ~loops.Stable
            flag_LMI_has_solution = false;
            if flag_lang
                warning('Получилась неустойчивая система');
            else
                warning('The closed-loop system is unstable!');
            end
            txt = input("Продолжить?/Continue? [y/n]","s");
            if ~strcmpi(txt,'y')
                break
            end
        end
    end
    %% Условие остановки
    if abs(mu_diff) < CS_param.tol
        % if(abs(StaticSens_norm_prev-StaticSens_norm) < CS_param.tol)
        if flag_lang
            fprintf('%s\n Регулятор успешно синтезирован: ∆ mu < %.2g%%\n%s\n',repmat('*',1,ncol(2)),CS_param.tol,repmat('*',1,ncol(2)));
        else
            fprintf('%s\n The controller has been successfully synthesized: ∆ mu < %.2g%%\n%s\n',repmat('*',1,ncol(2)),CS_param.tol,repmat('*',1,ncol(2)));
        end
        break;
    end
    if(iteration == CS_param.CCP_max_iter)
        if flag_lang
            fprintf('%s\n Достигнуто максимальное число итераций, регулятор условно синтезирован\n%s\n',repmat('*',1,ncol(2)),repmat('*',1,ncol(2)));
        else
            fprintf('%s\n Maximum number of iterations reached, controller conditionally synthesized\n%s\n',repmat('*',1,ncol(2)),repmat('*',1,ncol(2)));
        end
    end
    StaticSens_norm_prev = StaticSens_norm;
    disp(repmat('-',1,ncol(2)));
end

if  ~opt.keep_fig
    close(findobj(groot,'Tag','st_resp'));
end

if flag_lang
    if flag_LMI_has_solution
        fprintf('%s\n Завершено за %i итераций, процесс занял %.1f секунд.\n%s\n\n',repmat('=',1,ncol(1)),iteration,toc(whole_timer),repmat('=',1,ncol(1)));
    else
        fprintf('%s\n Прервано на %i итерации, процесс занял %.1f секунд.\n%s\n\n',repmat('=',1,ncol(1)),iteration,toc(whole_timer),repmat('=',1,ncol(1)));
    end
else
    if flag_LMI_has_solution
        fprintf('%s\n Completed in %i iterations. The process took %.1f seconds.\n%s\n\n',repmat('=',1,ncol(1)),iteration,toc(whole_timer),repmat('=',1,ncol(1)));
    else
        fprintf('%s\n Aborted at %i iteration. The process took %.1f seconds.\n%s\n\n',repmat('=',1,ncol(1)),iteration,toc(whole_timer),repmat('=',1,ncol(1)));
    end
end
