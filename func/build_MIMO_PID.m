%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function PID = build_MIMO_PID(Kp, Ki, Kd, param, freq)
%BUILD_MIMO_PID Возвращает передаточную функцию ПИД-регулятора заданного типа или значение передадаточной функции на выбранной частоте
% param.type - тип регулятора
% param.Ts   - sample time, если 0, то непрерывное время

tau = param.tau;
Ts  = param.Ts;



if nargin == 5 % если нужно значение
    flag = true;
else % если нужна передаточная функция
    flag = false;
end

if Ts ~=0 % Дискретное время
    if flag
        % z = exp(1j*freq*Ts);
        z = freq;
    else
        z = tf('z',Ts);
    end
    PID = Kp + Ki * IDform(z,Ts,param.IFormula) + Kd/(tau + IDform(z,Ts,param.DFormula));
else % Непрерывное время
    if flag
        % s = 1j*freq;
        s = freq;
    else
        s = tf('s');
    end

    switch param.filter
        case "default"
            PID = Kp + Ki/s + Kd*s*inv(tau*s+1);
        case "hi_freq"
            PID = (Kp+Ki/s+Kd*s)/((tau*s)^2/2 + tau*s + 1);
        otherwise
            error('Необходимо выбрать тип фильтра в непрерывном времени: default или hi_freq');
    end
end

    function out = IDform(z,Ts,type)
        switch type
            case "ForwardEuler"
                out = Ts/(z-1);
            case "BackwardEuler"
                out = (Ts*z)/(z-1);
            case "BackwardEuler2"
                out = (Ts/2*z*(z+1))/(z-1)^2;
            case "Trapezoidal"
                out = (Ts/2)*(z+1)/(z-1);
            otherwise
                error('Необходимо выбрать тип регулятора в дискретном времени: Forward или Backward или Trapez');
        end
    end
end
