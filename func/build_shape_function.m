%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [out] = build_shape_function(w,p)
%BUILD_SHAPE_FUNCTION Creating shape functions for closed-loop transfer functions
%   Detailed explanation goes here
arguments
    w = [];
    p.type {mustBeMember(p.type, {'Constant','Rectangle','Triangle','Trapezoid','Cheby','Cheby_H'})};
    p.Ts = 0;
    %
    p.H_inf_norm
    p.min_gain   = []
    p.unity_gain = 1
    %
    p.cross_freq_max = inf
    p.cross_freq_min = 0
    p.max_freq       = []
    p.min_freq       = 1e-6
    %
    p.alpha          = []
    p.trapez_freq     = []
    p.Cheby_order    = []
    %
    p.custom_freq    = []
    p.custom_data    = []
    %
    p.data_units {mustBeMember(p.data_units, {'mag','dB'})} = 'mag'
    p.freq_units {mustBeMember(p.freq_units, {'rad/s','Hz'})} = 'Hz'
end

switch p.freq_units
    case 'Hz'
        if p.Ts ~= 0
            p.max_freq = pi/p.Ts;
        else
            p.max_freq = p.max_freq         *2*pi;
        end
        p.min_freq       = p.min_freq       *2*pi;
        p.cross_freq_max = p.cross_freq_max *2*pi;
        p.cross_freq_min = p.cross_freq_min *2*pi;
        p.custom_freq    = p.custom_freq    *2*pi;
        p.trapez_freq     = p.trapez_freq     *2*pi;
    case 'rad/s'
        if p.Ts ~= 0
            p.max_freq = pi/p.Ts;
        else
            p.max_freq = p.max_freq        ;
        end
        p.min_freq       = p.min_freq      ;
        p.cross_freq_max = p.cross_freq_max;
        p.cross_freq_min = p.cross_freq_min;
        p.custom_freq    = p.custom_freq   ;
        p.trapez_freq     = p.trapez_freq    ;
    otherwise
        error("only 'rad/s' or 'Hz' is supported for gain boundary freq units")
end
p.freq_units = 'rad/s';

switch p.data_units
    case 'mag'
        p.H_inf_norm  = p.H_inf_norm;
        p.min_gain    = p.min_gain;
        p.unity_gain  = p.unity_gain;
        p.custom_data = p.custom_data;
    case 'dB'
        p.H_inf_norm  = db2mag(p.H_inf_norm);
        p.min_gain    = db2mag(p.min_gain);
        p.unity_gain  = db2mag(p.unity_gain);
        p.custom_data = db2mag(p.custom_data);
    otherwise
        error("only 'mag' or 'dB' is supported for gain boundary data units")
end
p.data_units = 'mag';

if isempty(w)
    out.freq = logspace(log10(p.min_freq),log10(p.max_freq),800);
    if (p.cross_freq_min ~= 0 && isfinite(p.cross_freq_max))
        out.freq = sort([out.freq p.cross_freq_min p.cross_freq_max]); % for best view
    end
else
    out.freq = w;
end
out.data = NaN(size(out.freq));
% out.data = ones(size(out.freq)).*p.unity_gain;

ind_min = (out.freq<p.cross_freq_min);
ind_max = (out.freq>p.cross_freq_max);
ind_mid = ~(ind_max | ind_min);

switch p.type
    case 'Constant'
        out.data = ones(size(out.data)).*p.H_inf_norm;
    case 'Rectangle'
        out.data(ind_min) = ones(size(out.data(ind_min))).*p.unity_gain;
        out.data(ind_mid) = ones(size(out.data(ind_mid))).*p.H_inf_norm;
        out.data(ind_max) = ones(size(out.data(ind_max))).*p.unity_gain;
    case 'Triangle'
        out.data(ind_min) = ones(size(out.data(ind_min))).*p.unity_gain;
        out.data(ind_mid) = ones(size(out.data(ind_mid))).*p.H_inf_norm;
        out.data(ind_max) = mod_butter_filter(out.freq(ind_max),p.cross_freq_max,p.alpha);
    case 'Trapezoid'
        out.data(ind_min) = ones(size(out.data(ind_min))).*p.unity_gain;
        out.data(ind_mid) = ones(size(out.data(ind_mid))).*p.H_inf_norm;
        ind_trapez = out.freq > p.trapez_freq;
        out.data(ind_max & ~ind_trapez) = mod_butter_filter(out.freq(ind_max & ~ind_trapez),p.cross_freq_max,p.alpha);
        out.data( ind_trapez) = ones(size(out.data(ind_trapez)))*(p.cross_freq_max./p.trapez_freq).^p.alpha;
    case 'Cheby'
        out.data  = Cheby_filter(out.freq,p.cross_freq_max,p.H_inf_norm,p.unity_gain,p.Cheby_order);
    case 'Cheby_H'
        out.data(ind_min) = ones(size(out.data(ind_min))).*p.unity_gain;
        out.data(ind_mid) = ones(size(out.data(ind_mid))).*p.H_inf_norm;
        out.data(ind_max) = Cheby_filter(out.freq(ind_max),p.cross_freq_max,p.H_inf_norm,p.unity_gain,p.Cheby_order);

    case 'Custom'
        out.data = interp1(p.custom_freq,p.custom_data,out.freq,'linear','extrap');
end

for name = fieldnames(p)'
    if isempty(p.(name{1}))
        p = rmfield(p,name{1});
    end
end

out.param = p;

    function gain = Cheby_filter(freq,cross_freq_max,H_inf_norm,unity_gain,Cheby_order)
        if isempty(Cheby_order)
            Cheby_order = 2;
        end
        e = ((1/(2-H_inf_norm)^2) - 1)^(1/2);
        gain  = 1./sqrt(1+e^2*cos(Cheby_order*acos(freq./cross_freq_max)).^2)+H_inf_norm+unity_gain-2;
    end
    function gain = mod_butter_filter(freq,cross_freq_max,alpha)
        gain  = (cross_freq_max./freq).^alpha;
    end
end

