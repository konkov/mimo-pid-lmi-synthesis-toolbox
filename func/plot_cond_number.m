%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fig] = plot_cond_number(result,ind,opt)
%PLOT_SIGMAS Summary of this function goes here
%   Detailed explanation goes here
arguments
    result
    ind = []
    opt.show_labels {mustBeNumericOrLogical(opt.show_labels)} = true
    opt.data_units {mustBeMember(opt.data_units, {'mag','dB'})} = 'mag'
    opt.freq_units {mustBeMember(opt.freq_units, {'rad/s','Hz'})} = 'Hz'
    opt.XLim = NaN
    opt.YLim = NaN
end
P = ss(result.P);

if isempty(ind)
    ind = length(result.PID);
end


Ts  = P.Ts;
q       = size(P,1);
N_model = size(P,3);

if N_model == 1
    n_label = '';
else
    n_label = '_n';
end
if q == 1
    q_label = '';
else
    q_label = '_i';
end

label_T     = sprintf('$\\sigma%s(T%s)$'   ,q_label,n_label);
label_inv_S = sprintf('$1/\\sigma%s(S%s)$',q_label,n_label);
label_inv_F = sprintf('$1/\\sigma%s(F%s)$',q_label,n_label);

% label_inv_F = sprintf('$1/\\overline%s{\\sigma}(F%s)$',n_label);
label_L      = sprintf('$\\sigma%s(L%s)$',q_label,n_label);
label_T_max  = sprintf('$b_T$');
label_inv_S_max  = sprintf('$1/b_S$');

label_cond_T = '$\kappa(T)$';





fig = figure;
ax = axes(fig);
ax.XScale = 'log';
ax.Box = 'on';
ax.YLabel.String = 'Singular values (dB)';
cmap = lines(7);
% cmap(1,:); % blue
% cmap(2,:); % red
% cmap(3,:); % yellow
% cmap(4,:); % purple
% cmap(5,:); % green
% cmap(6,:); % ligth blue
% cmap(7,:); % dark red

% cmap(5,:); % green
color_T      = cmap(4,:); % purple
color_S      = cmap(2,:); % red
color_L      = cmap(5,:); % green
color_cond_T = cmap(1,:); % blue



% w = logspace(-8,pi,500)/Ts; % Набор круговых частот
w = result.freq_arr;

switch opt.freq_units
    case 'Hz'
        f = w./(2*pi); % переход из рад/с в Гц
        ax.XLabel.String = 'Frequency (Hz)';
    case 'rad/s'
        f = w;
        ax.XLabel.String = 'Frequency (rad/s)';
end

switch opt.data_units
    case 'mag'
        dB_flag = false;
        ax.YScale = 'log';
        ax.YLabel.String = 'Singular values (mag)';
    case 'dB'
        dB_flag = true;
        ax.YScale = 'linear';
        ax.YLabel.String = 'Singular values (dB)';
end

hold(ax,'on')

cross_freq = NaN(N_model,q);

label_all = cell([]);
legend_flag = true;
for i_C = ind
    C   = ss(result.PID(i_C).lti);
    TFs = loopsens(P,C);
    % if Ts ~= 0
    %     F = (result.P*result.PID(i_C).Ki*Ts)\(tf([1 0],1,Ts)-1);
    % else
    %     F = (result.P*result.PID(i_C).Ki)\(tf([1 0],1));
    % end
    if Ts ~= 0
        F = (dcgain(result.P)*result.PID(i_C).Ki*Ts) \ tf([1 -1],1,Ts);
    else
        F = (dcgain(result.P)*result.PID(i_C).Ki)    \ tf([1  0],1);
    end
    for i = 1:1:N_model
        %     val_FR_T = freqresp(TFs{i}.To,f,'Hz'); % Значения частнотной передаточной ф-и T
        %     sv_T   = pagesvd(val_FR_T); % SVD
        [sv_T,~] = sigma(TFs.To(:,:,i),w); % сингулярные числа T(z) на выбранных частотах
        [sv_S,~] = sigma(TFs.So(:,:,i),w); % сингулярные числа S(z) на выбранных частотах
        [sv_F,~] = sigma(F(:,:,i),w); % % сингулярные числа SLFS(z) на выбранных частотах
        [sv_L,~] = sigma(TFs.Lo(:,:,i),w); % сингулярные числа L(z) на выбранных частотах

        
        [RGA] = get_RGA(TFs.To(:,:,i),w,freq_units="rad/s");



        for j = 1:1:q
            cross_freq(i,j) = interp1(sv_L(j,:), w, 1);
        end
        %
        %


        plot(ax,f,magORdb(sv_T,dB_flag), "Color",color_T);
        label_all = make_legend(label_all,label_T,q,legend_flag);
        %
        plot(ax,f,magORdb(1./sv_S,dB_flag), "Color",color_S);
        label_all = make_legend(label_all,label_inv_S,q,legend_flag);
        %
        plot(ax,f,magORdb(1./sv_F, dB_flag), "Color",color_S,"LineStyle",":");
        % plot(ax,f,magORdb(   sv_F, dB_flag), "Color",color_S,"LineStyle",":");
        label_all = make_legend(label_all,label_inv_F,q,legend_flag);

        plot(ax,f,magORdb(sv_L,dB_flag),"Color",color_L); % sigma (L)
        label_all = make_legend(label_all,label_L,q,legend_flag);

        plot(ax,f,magORdb(sv_T(1,:)./sv_T(end,:),dB_flag), "Color",color_cond_T); % Condition number
        label_all = make_legend(label_all,label_cond_T,1,legend_flag);

        % plot(ax,f,magORdb(RGA,dB_flag), "Color",'black',LineStyle=':'); % RGA
        % label_all = make_legend(label_all,label_cond_T,numel(RGA),legend_flag);


        legend_flag = false;
    end
end


plot(ax,f,magORdb(1./result.bound.S_max.data,dB_flag),"Color",color_S,"LineStyle","-.");
label_all = make_legend(label_all,label_inv_S_max,1,true);

plot(ax,f,magORdb(result.bound.T_max.data,dB_flag),"Color",color_T,"LineStyle","-.");
label_all = make_legend(label_all,label_T_max,1,true);




% ind = (sv_SS(1,:) < 1);
% area(ax,f(ind),magORdb(1./sv_SS(1,ind)),'FaceAlpha',0.1,'FaceColor',color_S,'LineStyle','none')

if strcmp(opt.freq_units,'Hz')
    cross_freq = cross_freq/(2*pi); % переход из рад/с в Гц
end


[min_cross_freq,max_cross_freq] = bounds(cross_freq,"all");

xline(ax,min_cross_freq,'k--', sprintf('Crossover = %3.2g %s', min_cross_freq,opt.freq_units)); % частота среза
if (max_cross_freq - min_cross_freq) > 1e-3
    xline(ax,max_cross_freq,'k--', sprintf('Crossover = %3.2g %s', max_cross_freq,opt.freq_units)); % частота среза
end
xline(ax,f(end),'k-',sprintf('Nyquist = %3.2g %s',f(end),opt.freq_units)); % частота Найквиста

constLines = findobj(ax.Children, 'Type','ConstantLine');

set(constLines, ...
    'LabelHorizontalAlignment','center', ...
    'LabelVerticalAlignment','bottom', ...
    'LabelOrientation','aligned' ...
    );
if ~opt.show_labels
    set(constLines, 'Label', '');
end


legend(ax,label_all,'Interpreter','latex');

hold(ax,'off');

ax.Box = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';

if ~isnan(opt.XLim)
    ax.XLim = opt.XLim;
else
    ax.XLim = [f(1) f(end)*2];
end
if ~isnan(opt.YLim)
    ax.YLim = opt.YLim;
end

end

function out = magORdb(in,flag)
if flag
    out = mag2db(in);
else
    out = in;
end
end

function out = make_legend(in,label,N,flag)
if flag
    out = horzcat(in,{label},repmat({''},1,N-1));
else
    out = horzcat(in,repmat({''},1,N));
end
end