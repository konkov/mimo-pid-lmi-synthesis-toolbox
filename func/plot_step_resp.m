%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              MIMO PID LMI-Synthesis Toolbox              %
% Copyright (c) 2023  Konkov Artem (konkov@physics.msu.ru) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fig] = plot_step_resp(result,opt)
%PLOT_STEP_RESP Summary of this function goes here
%   Detailed explanation goes here
arguments
    result
    opt.time_final = []
    opt.print = false    % флаг
    opt.mainFontsize = 8 % размер шрифта подписей
    opt.tickFontsize = 6 % размер шрифта меток
    opt.YLim         = []
end
i_C = length(result.PID);


C   = ss(result.PID(i_C).lti);
P = ss(result.P);
TFs = loopsens(P,C);

G = TFs.To;

if opt.print
    for i = 1:1:size(G,1)
        G.OutputName{i} = sprintf('$y_%i$',i);
    end
    for i = 1:1:size(G,2)
        G.InputName{i}  = sprintf('$r_%i$',i);
    end
end
G_arr = cell(size(G,3),1);
for i = 1:1:size(G,3)
    G_arr{i} = G(:,:,i);
end

fig = figure('Name','Step Response','Tag','st_resp');
ax = axes(fig);
fig.Name = sprintf('Step Response');
% h = stepplot(ax,TFs.To,max(SettlingTime(iteration,:)));
h = stepplot(ax,G_arr{:},opt.time_final);

if opt.print
    plot_opt = getoptions(h);
    % T.String = sprintf('Step Response interation # %i',iteration);
    plot_opt.Title.String = '';
    plot_opt.InputLabels.Interpreter = 'latex';
    plot_opt.OutputLabels.Interpreter = 'latex';

    plot_opt.InputLabels.FontSize  = opt.tickFontsize;
    plot_opt.OutputLabels.FontSize = opt.tickFontsize;
    plot_opt.OutputLabels.Color = [0 0 0];
    plot_opt.InputLabels.Color  = [0 0 0];

    plot_opt.XLabel.FontSize = opt.mainFontsize;
    plot_opt.YLabel.FontSize = opt.mainFontsize;

    plot_opt.TickLabel.FontSize = opt.tickFontsize;

    if ~isempty(opt.YLim)
        for i = 1:length(plot_opt.YLim)
            plot_opt.YLim{i} = opt.YLim;
        end
    end

    setoptions(h, ...
        'Title',plot_opt.Title, ...
        'XLabel',plot_opt.XLabel, ...
        'YLabel',plot_opt.YLabel, ...
        'OutputLabels',plot_opt.OutputLabels, ...
        'InputLabels',plot_opt.InputLabels, ...
        'TickLabel',plot_opt.TickLabel, ...
        'YLim',plot_opt.YLim);

end
plot_opt = getoptions(h);

end

