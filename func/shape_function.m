classdef shape_function
    %SHAPE_FUNCTION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        data
        freq
        param
    end
    
    methods
        function obj = shape_function(varargin)
            %SHAPE_FUNCTION Construct an instance of this class
            out = build_shape_function(varargin{:});
            obj.data  = out.data;
            obj.freq  = out.freq;
            obj.param = out.param;
        end
        
        function fig = plot(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            [fig] = plot_shape_function(obj);           
        end
    end
end

