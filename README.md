# MIMO PID LMI-synthesis Toolbox

#### This toolbox implements the synthesis of the discrete MIMO PID from the paper: https://doi.org/10.3390/math12060810

If you use our toolbox in your research, please consider citing this paper.

```
@article{Konkov2024,
  title = {Synthesis Methodology for Discrete MIMO PID Controller with Loop Shaping on LTV Plant Model via Iterated LMI Restrictions},
  volume = {12},
  ISSN = {2227-7390},
  url = {http://dx.doi.org/10.3390/math12060810},
  DOI = {10.3390/math12060810},
  number = {6},
  journal = {Mathematics},
  publisher = {MDPI AG},
  author = {Konkov,  Artem E. and Mitrishkin,  Yuri V.},
  year = {2024},
  month = mar,
  pages = {810}
}
```

It is also possible to synthesise continuous MIMO PID from the paper: https://doi.org/10.1002/rnc.3376

version 1.0 beta

Update and new examples will be coming soon.

Feedback, suggestions, and bug reports are welcome.


Requirements for using this toolbox:

- MATLAB version above R2020a with:
	- Control System Toolbox;
	- Robust Control Toolbox;


- Convex optimization framework: 
	 - YALMIP: https://github.com/yalmip/YALMIP
    
	or
	
	- CVX: https://github.com/cvxr/CVX
- SDP solver:
	- SDPT3 4.0 https://github.com/sqlp/sdpt3.git
 
  or 
  
 	- MOSEK https://www.mosek.com
	 
