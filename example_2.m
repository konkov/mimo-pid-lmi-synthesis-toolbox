%
% Discrete MIMO PID with Ts = 1 ms.


if 1 % load result
    MIMO_PID = load('example_2.mat').MIMO_PID;
else % syntesis
    Ts = 1e-3;
    a  = [1 1.05];
    b  = [1 1.1];
    G = ss();
    for i = 1:2
        A = [
            -0.08715  0.05202  0.07474  0.10778 -0.09686  0.01005;
            -0.05577 -0.10843  0.18912  0.02523  0.10641 -0.00345;
            -0.02615 -0.17539 -0.15452 -0.02143  0.00923 -0.24192;
            00.06087 -0.10741  0.01306 -0.25575  0.23213  0.02388;
            -0.07169  0.03582 -0.14195  0.17043 -0.2653  -0.14276;
            -0.12944 -0.06752  0.16983  0.16764 -0.03646 -0.1773
            ];
        B = [
            0        89.98 -21.3   98.35;
            0       -30.01   0    -29.77;
            -103.22   0   -104.31 114.37;
            0       -34.51 -27.01 -53.16;
            -41.89  101.28 -43.81   0;
            0         0    -40.87   0
            ];
        C = [
            0.9707*a(i)    0.9510*a(i)    0.7059    1.4580*b(i)   -1.2371    0.3174;
            00             0              1.4158    0.0475        -2.1935    0.4136;
            -0.4383*a(i)   0.6489*a(i)   -1.6045    1.7463*b(i)   -0.3334   -0.5771
            ];
        G(:,:,i) = ss(A,B,C,[]);
    end
    
    Gd = c2d(G,Ts,'zoh');

    fprintf('RGA = \n'); disp(get_RGA(Gd))

    S_max = 1.4;
    T_max = build_shape_function(type='Cheby_H',H_inf_norm=1.4,cross_freq_max=100,Cheby_order=2,Ts=Gd.Ts);
    % T_max = build_shape_function(type='Triangle',H_inf_norm=1.3,cross_freq_max=100,alpha=1.3,Ts=Gd.Ts);
    % T_max = build_shape_function(type='Trapezoid',H_inf_norm=1.3,cross_freq_max=100,trapez_freq=300,alpha=1.5,Ts=Gd.Ts);
    % plot_bounds(T_max);
    % Q_max = 20/min(svd(freqresp(Gd(:,:,1),0)));
    Q_max = 10;
    W_max = inf;

    MIMO_PID = LMI_MIMO_PID_syn( ...
        Gd, ...
        type="PID", ...
        CCP_max_iter = 11, ... % not more
        freq_N   = 300, ...
        freq_int = 1e-4, ...
        init_epsilon = 1e-2, ...
        S_max = S_max, ...
        T_max = T_max, ...
        Q_max = Q_max, ...
        W_max = W_max, ...
        detailed = true, ...
        verbose=false, ...
        packet='yalmip' , ...
        solver='sdpt3' , ...
        precision='low', ...
        lang='en' ...
        );
    % save('example_2.mat','MIMO_PID','-mat')
end

plot_sigmas(MIMO_PID,YLim=[1e-3 1e5],XLim=[5e-4 1e3],show_Q=true,show_W=true,show_labels=true);

plot_step_resp(MIMO_PID,print=false,time_final=0.05);

plot_CCP_results(MIMO_PID,"mu_scale","log");

